			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">


					<ul id="clubInfo">
						<li id="phoneNumber">215-913-4804 </li>
						<li id="email"><a href="mailto:anleyt@aol.com">anleyt@aol.com</a></li>
						<li id="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></li>
					</ul>
				

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
		
	</body>

</html>
