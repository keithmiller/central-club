<?php
/*
Template Name: Homepage Template
*/
?>

<?php get_header(); ?>

			<div id="content" class="homepage">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class=" first clearfix" role="main">
						<div id="homeHeader">
							<div id="homeImgWrap">
								<img id="homeImg" src="wp-content/themes/centralclub/library/images/wilsonGrandchildren2.jpg" alt="">
								<h3 id="ourMission">Our Mission</h3>
								<span class="clearfix"></span>
							</div>
						<div id="actionBtns">
							<div id="donateAction" class="action">
								<h1>Donate</h1>
								<img src="wp-content/themes/centralclub/library/images/tanArrow.png" class="arrow">
								<h3>Learn more about how you can contribute to this cause.</h3>
								<a href="http://keithmillerweb.com/centralclub/get-involved/donate/"><span></span></a>
							</div>
							<div id="volunteerAction" class="action">
								<h1>Volunteer</h1>
								<img src="wp-content/themes/centralclub/library/images/greenArrow.png" class="arrow">
								<h3>Learn more about how you can aid this cause.</h3>
								<a href="http://keithmillerweb.com/centralclub/get-involved/volunteer/"><span></span></a>
							</div>
						</div>
						</div>
						<span class="clearfix"></span>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title"><?php the_title(); ?></h1>
								

								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section>

								<footer class="article-footer">
									<p class="clearfix"><?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>

								</footer>


							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>
						<!--<p id="homeMission">
							Every man who has lotted here over the centuries, has looked up to the light and imagined climbing to freedom. So easy, so simple! And like shipwrecked men turning to seawater foregoing uncontrollable thirst, many have died trying. And then here there can be no true despair without hope. So as I terrorize Gotham, I will feed its people hope to poison their souls. I will let them believe that they can survive so that you can watch them climbing over each other to stay in the sun. You can watch me torture an entire city. And then when you've truly understood the depth of your failure, we will fulfill Ra's Al Ghul's destiny. We will destroy Gotham. And then, when that is done, and Gotham is... ashes Then you have my permission to die. <a href="#">Read More&raquo;</a>
						</p>-->
						
						<hr>

						<div id="homeEvents">
						<h1 class="upComing">Upcoming Events</h1>
						<?php $my_query = new WP_Query('category_name=Events&posts_per_page=3');
  							while ($my_query->have_posts()) : $my_query->the_post(); ?>

							<div class="blogItem">
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

								<div class="itemDate">
									<span class="month"><?php the_field('event_month'); ?></span>
									<span class="day"><?php the_field('event_day'); ?></span>
								</div>

		  						<div class="excerpt">
			  						<h4 class="itemTitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
									<section class="entry-content clearfix">
										<?php the_field('event_description'); ?>
									</section>
								</div>

								<footer class="article-footer">
									<p class="tags"><?php the_tags( '<span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>

								</footer>

						

							</article>
							</div>
							<?php endwhile; ?>
							</div>

							<div class="testimonial">
							<?php
							///////////// Testimonial ////////////////////
							 $my_query = new WP_Query('category_name=Testimonial&posts_per_page=1');
							
  							while ($my_query->have_posts()) : $my_query->the_post(); ?>

							
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">


		  						<div class="quote">
									<section class="entry-content clearfix">
										<?php the_field('testimonial_quotation'); ?>
									</section>
								</div>
								<div class="qAuthor">
										<?php the_field('quotation_author'); ?>
								</div> 
								<footer class="article-footer">
									<p class="tags"><?php the_tags( '<span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>

								</footer>
							</article>
							<?php endwhile; ?>
							</div>

						

						

				</div>

			</div>

<?php get_footer(); ?>
