<?php
/*
Template Name: Landing Template

Description: This template is used for parent pages. Child pages will show up with an accompanying image, description and link. 
*/
?>

<?php get_header(); ?>

	<div id="content">
        				<div id="inner-content" class="wrap clearfix">

						<div id="main" class=" first clearfix" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

 <ul>       
<?php
	$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'asc' ) );

	foreach( $mypages as $page ) {		
		$content = $page->post_excerpt;
		if ( ! $content ) // Check for empty page
			continue;

		$content = apply_filters( 'the_content', $content );
	?>
		<li><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo get_the_post_thumbnail( $page->ID, 'thumbnail', array('class'=>"landingThumbnail") ); ?></a>
<div class="landingDescription">    
    <h2><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></h2>
		<?php echo $content; ?><p><a href="<?php echo get_page_link( $page->ID ); ?>">Read More</a></p></div></li>
		<br class="clear">
	<?php
	}	
?></ul>

	</div></div>
</div><!-- Page Div -->

<?php get_footer(); ?>
